#ifndef DI_DEBUG_H
#define DI_DEBUG_H

#include <ctype.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static inline void
hex_dump(const void* _bytes, size_t sz, size_t width, int ofd)
{
  if (!(_bytes && sz)) {
    return;
  }

  if (!width || width % 8) {
    width = 16;
  }

  const size_t line_sz = 8 + 2 + 4 * width + ((width + 8) / 8) * 2 + 1;
  char *ln = (char*)calloc(line_sz, sizeof *ln), *line = ln;
  if (!ln) {
    return;
  }
  const uint8_t* const bytes = (const uint8_t*)_bytes;
  size_t it;
  for (it = 0; it < sz; it += width) {
    size_t jt, pos = 0;
    pos += snprintf(line + pos, line_sz - pos, "%08zX", it);
    for (jt = it; jt < (it + width < sz ? it + width : sz); jt++) {
      pos += snprintf(
        line + pos, line_sz - pos, "%s%02X", (jt % 8) ? " " : "  ", bytes[jt]);
    }

    for (; jt % width; jt++) {
      pos += snprintf(
        line + pos, line_sz - pos, "%2c%s", ' ', (jt % 8) ? " " : "  ");
    }

    pos += snprintf(line + pos, line_sz - pos, "  %c", '|');
    for (jt = it; jt < (it + width < sz ? it + width : sz); jt++) {
      if (isalnum(bytes[jt])) {
        pos += snprintf(line + pos, line_sz - pos, "%c", bytes[jt]);
      } else {
        pos += snprintf(line + pos, line_sz - pos, "%c", '.');
      }
    }

    for (; jt % width; jt++) {
      pos += snprintf(line + pos, line_sz - pos, "%c", ' ');
    }

    pos += snprintf(line + pos, line_sz - pos, "%c\n", '|');
    write(ofd, ln, line_sz);
    line = ln;
  }
  free(ln);
}

static inline void
__di_log(const char* _level,
         const char* _fname,
         unsigned _lno,
         int _ofd,
         const char* _format,
         ...)
{
  char* buff = NULL;
  size_t loc = 0;
  FILE* s = open_memstream(&buff, &loc);
  if (!s) {
    return;
  }
  fprintf(s, "[%s] %s:%u:\n\t", _level, _fname, _lno);
  va_list vlist;
  va_start(vlist, _format);
  vfprintf(s, _format, vlist);
  va_end(vlist);
  fclose(s);
  write(_ofd, buff, loc);
  free(buff);
}

#ifdef __cplusplus
#define __di_fspec __PRETTY_FUNCTION__
#else
#define __di_fspec __func__
#endif

#ifndef DIL_DBG
#define DIL_DBG(format, ...)                                                   \
  do {                                                                         \
    __di_log(                                                                  \
      "DEBUG", __di_fspec, __LINE__, STDERR_FILENO, format, ##__VA_ARGS__);    \
  } while (0)
#endif

#ifndef DIL_ERR
#define DIL_ERR(format, ...)                                                   \
  do {                                                                         \
    __di_log(                                                                  \
      "ERROR", __di_fspec, __LINE__, STDERR_FILENO, format, ##__VA_ARGS__);    \
  } while (0)
#endif

#endif /* DI_DEBUG_H */
