#ifndef DI_DEBUG_HPP
#define DI_DEBUG_HPP
#include <algorithm>
#include <cstdarg>
#include <cstring>
#include <exception>
#include <iostream>
#include <iterator>
#include <sstream>

#include "di_debug.h"

/**
 * @brief Used by VARDUMP to pattern match recursive call final condition - no
 * more arguments
 *
 * @param[in] it  Iterator holding no more arguments
 */
static void err(std::istream_iterator<std::string> /*it*/)
{
  std::cerr << std::endl;
}

/**
 * @brief Used by VARDUMP to recursively print variable values given by the
 * user.
 *
 * @tparam T    Type of current variable
 * @tparam Args List of types of remaining variables
 * @param it    List of remaining variable names
 * @param a     Current variable value
 * @param args  List of remaining variable values
 */
template<typename T, typename... Args>
static void
err(std::istream_iterator<std::string> it, T a, Args... args)
{
  std::cerr << "\n" << *it << " = " << a;
  err(++it, args...);
}

/**
 * @brief Macro used to print variables and their values to stderr - tricky
 * internet hack...tread wisely
 *
 * @param args...   Variables to get printed.
 *
 */
#define VARDUMP(args...)                                                       \
  {                                                                            \
    std::string _s = #args;                                                    \
    std::replace(_s.begin(), _s.end(), ',', ' ');                              \
    std::stringstream _ss(_s);                                                 \
    std::istream_iterator<std::string> _it(_ss);                               \
    err(_it, args);                                                            \
  }

#endif // DI_DEBUG_HPP
